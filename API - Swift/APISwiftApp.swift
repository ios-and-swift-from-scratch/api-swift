//___FILEHEADER___

import SwiftUI

@main
struct APISwift: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
