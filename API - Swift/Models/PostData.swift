//
//  PostData.swift
//  API - Swift
//
//  Created by Md. Ebrahim Joy on 14/1/24.
//

import Foundation

struct Results : Decodable {
    let hits: [Post]
}

struct Post : Decodable, Identifiable {
    var id: String {
       return objectID
    }
    let objectID: String
    let points: Int
    let title: String
    let url: String?

}
